<?php

require_once'models/categoria.php';
require_once'models/producto.php';


class categoriaController{

    public function index(){
        Utils::isAdmin();
        $categoria = new  Categoria();
        $categorias =$categoria->getALL();
        require_once'views/categoria/index.php';
        

    }

//ver categorias
    public function ver(){
        if(isset($_GET['id'])){
            //var_dump($_GET['id']);
            $id =$_GET['id'];

            //conseguir categoria
            $categoria = new Categoria();
            $categoria->setId($id);
            $categoria =$categoria->getOne();
             //conseguir productos
             $producto = new Producto();
             $producto->setCategoria_id($id);
             $productos = $producto->getALLCategory();
        }
        require_once'views/categoria/ver.php';
       

    }
//crear categoria
    public function crear(){
        Utils::isAdmin();
        require_once'views/categoria/crear.php';


    }
//guardar categoria    
    public function save(){
        Utils::isAdmin();
        if(isset($_POST) && isset($_POST['nombre'])){
            //guardar la categoria en db
            $categoria = new Categoria();
            $categoria->setNombre($_POST['nombre']);
            $categoria->save();
        }
        header("Location:".base_url."categoria/index");
    }
}
?>