<?php
require_once'models/producto.php';

class productoController{

    public function index(){
       // echo "Controlador productos,Accion index";
       //renderizar vista
       $producto = new Producto();
       $productos =$producto->getRandom(15);
       require_once 'views/producto/destacados.php';
       
       
    }
    /*Mostar productos para mujeres */
    public function mostrar(){
    $producto = new Producto();
    $productosy =$producto->getRandom_2(6);
    require_once 'views/producto/destacados.php';
    
    
    }



//ver productos endividuales
    public function ver(){
        if(isset($_GET['id'])){
            $id=$_GET['id'];
            
    
            $producto =new Producto();
            $producto->setId($id);
            $product =$producto->getOne();
        }
        require_once 'views/producto/ver.php';



    }
//gestionar productos
    public function gestion(){
        Utils::isAdmin();
        $producto = new Producto();
        $productos =$producto->getALL();

        require_once 'views/producto/gestion.php';
      
    }
 //crear productos   
    public function crear(){
        Utils::isAdmin();
      

        require_once 'views/producto/crear.php';

    } 
 //guardar productos   
    public function save(){
        Utils::isAdmin();
        if(isset($_POST)){
            //var_dump($_POST);
            $nombre =isset($_POST['nombre']) ? $_POST['nombre'] :false;
            $descripcion =isset($_POST['descripcion']) ? $_POST['descripcion'] :false;
            $precio =isset($_POST['precio']) ? $_POST['precio'] :false;
            $stock =isset($_POST['stock']) ? $_POST['stock'] :false;
            $categoria =isset($_POST['categoria']) ? $_POST['categoria'] :false;
            $subcategoria =isset($_POST['subcategoria']) ? $_POST['subcategoria'] :false;
            //$imagen =isset($_POST['imagen']) ? $_POST['imagen'] :false;
       
            if($nombre && $descripcion && $precio && $stock && $categoria && $subcategoria ){ 
                $producto =new Producto();
                $producto->setNombre($nombre);
                $producto->setDescripcion($descripcion);
                $producto->setPrecio($precio);
                $producto->setStock($stock);
                $producto->setCategoria_id($categoria);
                $producto->setSubcategoria_id($subcategoria);
               // var_dump($producto); 
               // die;
                //Guardar la imagen 0
                if(isset($_FILES['imagen'])){
                    $file =$_FILES['imagen'];
                    $filename =$file['name'];
                    $mimetype =$file['type'];
    
                    if($mimetype == "image/jpg" || $mimetype =="image/jpeg"  || $mimetype =="image/png"  || $mimetype =="image/webp"){
                      //  var_dump($file); 
    
                        //die;
                        if(!is_dir('uploads/images')){
                            mkdir ('uploads/images',777 ,true);
                        }
                      
                        move_uploaded_file($file['tmp_name'],'uploads/images/'.$filename);
                
                        //imgen n0
                        $producto->setImagen($filename);
                    
                    
    
                    }
                    

                }
                 //Guardar la imagen 1
                if(isset($_FILES['imagen1'])){
                    $file =$_FILES['imagen1'];
                    $filename =$file['name'];
                    $mimetype =$file['type'];
    
                    if($mimetype == "image/jpg" || $mimetype =="image/jpeg"  || $mimetype =="image/png"  || $mimetype =="image/webp"){
                      //  var_dump($file); 
    
                        //die;
                        if(!is_dir('uploads/images')){
                            mkdir ('uploads/images',777 ,true);
                        }
                      
                        move_uploaded_file($file['tmp_name'],'uploads/images/'.$filename);
                        //imgen n1
                        $producto->setImagen1($filename);
                    }
                    
                }
                  //Guardar la imagen 2
                  if(isset($_FILES['imagen2'])){
                    $file =$_FILES['imagen2'];
                    $filename =$file['name'];
                    $mimetype =$file['type'];
    
                    if($mimetype == "image/jpg" || $mimetype =="image/jpeg"  || $mimetype =="image/png"  || $mimetype =="image/webp"){
                      //  var_dump($file); 
    
                        //die;
                        if(!is_dir('uploads/images')){
                            mkdir ('uploads/images',0777 ,true);
                        }
                      
                        move_uploaded_file($file['tmp_name'],'uploads/images/'.$filename);
                        //imgen n2
                        $producto->setImagen2($filename);
                    }
                    
                }   
                
                if(isset($_GET['id'])){
                    $id =$_GET['id'];
                    $producto->setId($id);

                    $save =$producto->edit();

                }else{
                    $save =$producto->save();

                }
                if($save){
                    $_SESSION['producto']="complete";
                }else{
                    $_SESSION['producto']="failed";
                }
            }else{
                $_SESSION['producto']="failed";
            }
        }else{
            $_SESSION['producto']="failed";

        }
        header('Location:'.base_url.'producto/gestion');


    }
// Editar Productos
    public function editar(){
       // var_dump($_GET);
       Utils::isAdmin();

       if(isset($_GET['id'])){
        $id=$_GET['id'];
        $edit=true;

        $producto =new Producto();
        $producto->setId($id);
        $pro =$producto->getOne();

        require_once'views/producto/crear.php';
       }else{
        header('Location:'.base_url.'producto/gestion');

       }

    }
//Eliminar producto    
    public function eliminar(){
        Utils::isAdmin();

        if(isset($_GET['id'])){
            $id=$_GET['id'];
            $producto =new Producto();
            $producto->setId($id);

            $delete = $producto->delete();
            if($delete){
                $_SESSION['delete']='complete';
            }else{
                $_SESSION['delete']='failed';
            }
        }else{
            $_SESSION['delete']='failed';
        }
        header('Location:'.base_url.'producto/gestion');

    }
    
}
?>