<?php
require_once'models/pedido.php';
class pedidoController{
    public function hacer(){
       // echo "Controlador pedidos,Accion index";
       require_once'views/pedido/hacer.php';
    }
//llenado de datos del pedido
    public function add(){
     // var_dump($_POST);
       // die;
        if(isset($_SESSION['identity'])){

            $usuario_id =$_SESSION['identity']->id;
            $nombrecompleto = isset($_POST['nombrecompleto']) ? $_POST['nombrecompleto']:false;
            $identidad = isset($_POST['identidad']) ? $_POST['identidad']:false;
            $celular = isset($_POST['celular']) ? $_POST['celular']:false;
            $localidad = isset($_POST['localidad']) ? $_POST['localidad']:false;
            $domicilio = isset($_POST['domicilio']) ? $_POST['domicilio']:false;
            $direccion= isset($_POST['direccion']) ? $_POST['direccion']:false;
      
            
            $stats =Utils::statsCarrito();
            $coste =$stats['total'];

              if($nombrecompleto && $identidad && $celular && $localidad && $domicilio && $direccion){
                 //Guardar datos db
                $pedido =new Pedido();
                $pedido->setUsuario_id($usuario_id);
                $pedido->setNombrecompleto($nombrecompleto);
                $pedido->setIdentidad($identidad);
                $pedido->setCelular($celular);
                $pedido->setLocalidad($localidad);
                $pedido->setDomicilio($domicilio);
                $pedido->setDireccion($direccion);
                $pedido->setCoste($coste);


               // var_dump($pedido);
                //die;

                $save =$pedido->save();
                //Guardar linea pedido
                $save_linea = $pedido->save_linea();

                if($save &&  $save_linea){
                    $_SESSION['pedido']="complete";
                }else{
                    $_SESSION['pedido']="failed";
                }

              }else{
                $_SESSION['pedido']="failed";
              }
              header("Location:".base_url.'pedido/confirmado');
            
        }else{
            //Redigir al index
            header("Location:".base_url);
        }

    }
 //pedido cofirmado por el usuario   
    public function confirmado(){
        if(isset($_SESSION['identity'])){
            $identity = $_SESSION['identity'];
            $pedido = new Pedido();
            $pedido->setUsuario_id($identity->id);

            $pedido =$pedido->getOneByUser();

            $pedido_productos =new Pedido();
            $productos =$pedido_productos->getProductosByPedido($pedido->id);

        }
        require_once'views/pedido/confirmado.php';
    }

//codigo de mis pedidos
    public function mis_pedidos(){
        Utils::isIdentity();

        $usuario_id = $_SESSION['identity']->id;
        $pedido =new Pedido();

        //sacar los pedidos del usuario
        $pedido->setUsuario_id($usuario_id);
        $pedidos = $pedido->getALLByUser();

        require_once'views/pedido/mis_pedidos.php';
    }

//sacar detalle del pedidos del usuario

    public function detalle(){
        Utils::isIdentity();

        if(isset($_GET['id'])){
            $id =$_GET['id'];

            $pedido = new Pedido();
            $pedido->setId($id);

            $pedido =$pedido->getOne();
            //sacer los productos de pedido
            $pedido_productos =new Pedido();
            $productos =$pedido_productos->getProductosByPedido($id);

            require_once'views/pedido/detalle.php';
        }else{
            header('Location:'.base_url.'pedido/mis_pedidos');
        }
//gestionar pedidos a admin y user
    }
    public function gestion(){
        Utils::isAdmin();
        $gestion = true;

        $pedido = new Pedido();
        $pedidos =$pedido->getALL();

        require_once'views/pedido/mis_pedidos.php';

    }

//canbiar el estado de pedido
    public function estado(){
        Utils::isAdmin();

        if(isset($_POST['pedido_id']) && isset($_POST['estado'])){
            //recoger datos form
            $id = $_POST['pedido_id'];
            $estado = $_POST['estado'];
            //update del pedido
            $pedido = new Pedido();
            $pedido->setId($id);
            $pedido->setEstado($estado);
            $pedido->edit();
           //  var_dump($pedido);
           //  die;
            header("Location:".base_url.'pedido/detalle&id='.$id);

        }else{
            header("Location:".base_url);
        }
    }
    
}
?>