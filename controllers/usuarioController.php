<?php
require_once 'models/usuario.php';

class usuarioController{
    public function index(){
        //echo "Controlador Usuarios,Accion index";
    }
    public function registro(){
        require_once 'views/usuario/registro.php';
    }
    //perfil de usuario
    public function perfil(){
        require_once 'views/usuario/perfil.php';
    }
    public function save(){
       if(isset($_POST)){
           $nombres= isset($_POST['nombres']) ? $_POST['nombres']:false;
           $apellidos= isset($_POST['apellidos']) ? $_POST['apellidos']:false;
           $email= isset($_POST['email']) ? $_POST['email']:false;
           $celulaIdentidad= isset($_POST['celulaIdentidad']) ? $_POST['celulaIdentidad']:false;
           $celular= isset($_POST['celular']) ? $_POST['celular']:false;
           $password= isset($_POST['password']) ? $_POST['password']:false;
        if($nombres && $apellidos && $email && $celulaIdentidad && $celular && $password){
           $usuario=new Usuario();
           $usuario->setNombres($nombres);
           $usuario->setApellidos($apellidos);
           $usuario->setEmail($email);
           $usuario->setCelulaIdentidad($celulaIdentidad);
           $usuario->setCelular($celular);
           $usuario->setPassword($password);

          
          $save= $usuario->save();
           if($save){
               $_SESSION['register']="complete";
           }else{
            $_SESSION['register']="failed";
           }
        }else{
            $_SESSION['register']="failed";
        }
       }
       else{
        $_SESSION['register']="failed post";
       }
       header("Location:".base_url.'usuario/registro');
    }

    public function login(){
        if(isset($_POST)){
            //identificar al usuario
            //consulta a la base de datos
            $usuario = new Usuario();
            $usuario->setEmail($_POST['email']);
            $usuario->setPassword($_POST['password']);
            
            $identity =$usuario->login();
          
            if($identity && is_object($identity)){
                $_SESSION['identity']=$identity;
                //si user es adim
               if($identity->rol =='admin'){
                    $_SESSION['admin']=true;

                }
             header("Location:".base_url);

            }
            else{
                //error no informa al user  el error de login
               $_SESSION['error_login']='Identificacion fallida !!';

               header("Location:".base_url.'usuario/registro');
               
                
               

            }
           
           // var_dump($identity);
           // die();
        }
       
     
     

    }
    public function logout(){
        if(isset($_SESSION['identity'])){
            unset($_SESSION['identity']);
        }
        //cerrar session de admin
            if(isset($_SESSION['admin'])){
                unset($_SESSION['admin']);
            }
        header("Location:".base_url);
    }
}

?>
