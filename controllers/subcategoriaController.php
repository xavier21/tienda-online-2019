<?php
    require_once'models/subcategoria.php';
    require_once'models/producto.php';

class subcategoriaController{
  
   
    public function index(){
        Utils::isAdmin();
          // echo "Controlador subcategoria,Accion index";
          $subcategoria = new  Subcategoria();
          $subcategorias =$subcategoria->getALL();
          
  
        require_once'views/subcategoria/index.php';

    }


    public function crear(){
        Utils::isAdmin();
        require_once'views/subcategoria/crear.php';


    }
//ver productos endividuales
    public function ver(){
            if(isset($_GET['id'])){
                $id =$_GET['id'];

                //conseguir el nombre de subcategoria
                $subcategoria = new Subcategoria();
                $subcategoria->setId($id);
                $subcategoria =$subcategoria->getOne();
                
                //sacar productos  segun su sub categoria
                $producto = new Producto();
                $producto->setSubcategoria_id($id);
                $productos = $producto->getALLSubcategory();
            }
            require_once 'views/subcategoria/ver.php';
    
    
    
    }

//guardar subcategoria
    public function save(){
        Utils::isAdmin();
        if(isset($_POST) && isset($_POST['categoria'])){
            //guardar la categoria en db
            $subcategoria = new Subcategoria();
            $subcategoria->setCategoria_Id($_POST['categoria']);
            $subcategoria->setNombre($_POST['nombre']);
            $subcategoria->save();
        }
        header("Location:".base_url."subcategoria/index");
    }
}


?>