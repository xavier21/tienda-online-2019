<?php
class Utils{
    public static function deleteSession($name){
        if(isset($_SESSION[$name])){
            $_SESSION[$name]=null;
            unset($_SESSION[$name]);
        }
        return $name;
    }

    public static function isAdmin(){
        if(!isset($_SESSION['admin'])){
         header("Location:".base_url);
        }else{
            return true;
        }
    }
    //codigo si el usuario esta logeado
    public static function isIdentity(){
        if(!isset($_SESSION['identity'])){
         header("Location:".base_url);
        }else{
            return true;
        }
    }
    public static function showCategorias(){
        require_once'models/categoria.php';
        $categoria = new  Categoria();
        $categorias =$categoria->getALL();
        return $categorias;

    }
    //subcategorias 
    public static function showSubcategorias(){
        require_once'models/subcategoria.php';
        $subcategoria = new  Subcategoria();
        $subcategorias =$subcategoria->getALL();
        return $subcategorias;

    }
    //mostrar productos en ver
    public static function showproducto(){
        require_once'models/producto.php';
        $producto = new Producto();
        $productos =$producto->getRandom(10);
        return $productos ;

    }
 

    public static function statsCarrito(){
        $stats =array(
            'count' => 0,
            'total' => 0,
        );
        if(isset($_SESSION['carrito'])){
            $stats['count'] = count($_SESSION['carrito']);

            foreach($_SESSION['carrito'] as $producto){
                $stats['total'] +=$producto['precio']*$producto['unidades'];
            }
        } 
        return $stats;
    }
    //canbiar estado de envio 
    public static function showStatus($status){
        $value ='Pendiente';

        if($status == 'confirm'){
            $value = 'Pendiente';
        }elseif($status =='preparation'){
            $value = 'En Preparacion';
        }elseif($status =='ready'){
            $value = 'Preparado para enviar';
        }elseif($status ='sended'){
            $value = 'Eviado';
        }

        return $value;

    }
    //mis pedidos
    public static function showpedidos(){
        require_once'models/pedido.php';
        $usuario_id = $_SESSION['identity']->id;
        $pedido =new Pedido();

        //sacar los pedidos del usuario
        $pedido->setUsuario_id($usuario_id);
        $pedidos = $pedido->getALLByUser();
        return $pedidos;
    
    }
}

?>