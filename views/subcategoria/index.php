<div class="container-card">
    <div class=admin-h2>
        <h2>Gestionar Sub-Categorias</h2>
    </div>
    <div class="btn-crear">
        <a href="<?=base_url?>subcategoria/crear"><span class="icon-edit"></span></a>
    </div>
</div>
<div class="table-admi">
    <table border="1">
        <thead>
            <tr>
                <th>ID</th>
                <th>CATEGORIA</th>
                <th>NOMBRE</th>
            </tr>
        </thead>
        <?php while($subcat = $subcategorias->fetch_object()):?>
        <tbody>
            <tr>
                <td><?=$subcat->id;?></td>
                <td><?=$subcat->categoria_id;?></td>
                <td><?=$subcat->nombre;?></td>
            </tr>

            <?php endwhile; ?>
        </tbody>
    </table>
</div>