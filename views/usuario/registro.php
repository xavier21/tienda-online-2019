<div class=" login-registro">
    <?php if(isset($_SESSION['register'])&&$_SESSION['register']=='complete'):?>
    <strong>Registro Completado Correctamente</strong>
    <?php elseif(isset($_SESSION['register'])&&$_SESSION['register']=='failed'):?>
    <strong>Registro failed,rellene los campos de formulario</strong>
    <?php endif;?>
    <?php Utils::deleteSession('register'); ?>

    <div class="tab-login">
        <button class="tablinks" onclick="openCity(event, 'login')" id="defaultOpen">INICIA SESION</button>
        <button class="tablinks" onclick="openCity(event, 'registro')">REGISTRATE</button>
    </div>
    <div class="tabcontent-p" id="login">

        <?php if(!isset($_SESSION['identity'])):?>

        <form action="<?=base_url?>usuario/login" method="post">
            <label>Correo Electronico</label>
            <input type="email" name="email" required />
            <label>Contraseña</label>
            <input type="password" name="password" placeholder="contaseña" required>
            <input type="submit" value="INICIA SESION">
        </form>
        <?php else: ?>
        <h3><?=$_SESSION['identity']->nombres?> <?=$_SESSION['identity']->apellidos?></h3>
        <?php endif; ?>

    </div>
    <div class="tabcontent-p" id="registro">
        <form action="<?=base_url?>usuario/save" method="POST">
            <label>Nombres</label>
            <input type="text" name="nombres" placeholder="Nombres">
            <label>Apellidos</label>
            <input type="text" name="apellidos" placeholder="Apellidos">
            <label>Correo Electronico</label>
            <input type="email" name="email" placeholder="@gmail.com" required />
            <label>Numero de C.I</label>
            <input type="text" name="celulaIdentidad" placeholder="Nombres">

            <label>Numero de Celular</label>
            <input type="text" name="celular" placeholder="">
            <label>Contraseña</label>
            <input type="password" name="password" placeholder="">
            <p>Al crear una cuenta estás aceptando el Acepto los términos y condiciones de AmaxExpress.com y Política de
                Privacidad</p>

            <input type="submit" value="Registrarse">
        </form>
    </div>


</div>
<script>
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent-p");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>