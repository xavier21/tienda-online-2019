<div class="user-img">
    <img src="<?=base_url ?>/img/icom/user.png" alt="">
    <p><?=$_SESSION['identity']->nombres?> <?=$_SESSION['identity']->apellidos ?> <samp> <a
                href="<?=base_url?>usuario/logout">Salir</a></samp></p>
</div>


<div class="container-perfil">
    <div class="tab">
        <button class="tablinks" onclick="openCity(event, 'Pedidos')" id="defaultOpen">Pedidos</button>
        <button class="tablinks" onclick="openCity(event, 'Cupones')">Cupones</button>
        <button class="tablinks" onclick="openCity(event, 'Entrega')">Dirección </button>
        <button class="tablinks" onclick="openCity(event, 'Cuenta')">Configuración</button>
        <button class="tablinks"><a href="<?=base_url?>usuario/logout">Salir</a></button>

    </div>
    <div id="Pedidos" class="tabcontent-p">
        <h3>Mis pedidos</h3>
        <table>
            <thead>
                <tr>
                    <th>N° Pedido</th>
                    <th>Fecha</th>
                    <th>Precio total</th>
                    <th>Estado</th>
                </tr>
            </thead>
            <tbody>
                <?php $pedidos =Utils::showpedidos();?>
                <?php
                 while($ped = $pedidos->fetch_object()):
                ?>
                <tr>
                    <td><a href="<?=base_url?>pedido/detalle&id=<?=$ped->id?>"><?=$ped->id?></a></td>
                    <td><?=$ped->fecha?></td>
                    <td><?=$ped->coste?> Bs</td>

                    <td><?= Utils::showStatus($ped->estado)?></td>
                </tr>
                <?php endwhile;?>
            </tbody>
        </table>
    </div>

    <div id="Cupones" class="tabcontent-p">
        <h3>Mis cupones</h3>
        <p>No hay Cupones</p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum similique minus quos a tempora iusto quidem
            dignissimos velit, maxime temporibus suscipit voluptas doloribus dolorem adipisci possimus consectetur at
            quia ad!</p>
    </div>

    <div id="Entrega" class="tabcontent-p">
        <h3>Mi dirección de entrega</h3>
        <div class="container-dat-envio-perf">

            <div class="info-envio">
                <ul class="data-envio">
                    <li>Nombre completo:</li>
                    <li>Contacto:</li>
                    <li>Identidad:</li>
                    <li>Celular:</li>
                    <li>Codigo:</li>
                </ul>
            </div>
            <div class="">
                <ul class="data-envio">
                    <li>javier castillo rosa</li>
                    <li>Calle:10 de noviembre Barrio: 14 de sectiembre Municipo:entre rios</li>
                    <li>12345678</li>
                    <li>12345678</li>
                    <li>X-01</li>
                </ul>
            </div>
        </div>

    </div>
    <div id="Cuenta" class="tabcontent-p">
        <h3>Configuración de la cuenta</h3>
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Minima, maxime, nam tenetur totam voluptatibus
            consequuntur eos commodi tempore modi neque laboriosam voluptatem ratione culpa possimus dolores non sunt
            nobis error!</p>
    </div>

</div>
<script>


function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent-p");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();


</script>