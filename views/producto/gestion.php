<div class="container-card">
    <div class=admin-h2>
        <h2>Gestionar productos</h2>
    </div>
    <div class="btn-crear">
        <a href="<?=base_url?>producto/crear"><span class="icon-edit"></span></a>
    </div>

</div>

<?php if(isset($_SESSION['producto']) && $_SESSION['producto'] =='complete'):?>
<strong>El producto se ha creado corectamente</strong>
<?php elseif(isset($_SESSION['producto']) && $_SESSION['producto'] =='failed'):?>
<strong>El producto no se ha creado corectamente</strong>
<?php endif;?>
<?php Utils::deleteSession('producto'); ?>
<!--borrar productos aler  -->
<?php if(isset($_SESSION['delete']) && $_SESSION['delete'] =='complete'):?>
<strong>El producto se ha borrado corectamente</strong>
<?php elseif(isset($_SESSION['delete']) && $_SESSION['delete'] =='failed'):?>
<strong>El producto no se ha borrado corectamente</strong>
<?php endif;?>
<?php Utils::deleteSession('delete'); ?>
<div class="table-admi">
    <table border="1">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>PRECIO</th>
                <th>STOCK</th>
                <th>ACCIONES</th>

            </tr>
        </thead>
        <tbody>
            <?php while($pro =$productos->fetch_object()):?>
            <tr>
                <td><?=$pro->id;?></td>
                <td><p class="text-producto"><?=$pro->nombre;?></p></td>
                <td><?=$pro->precio;?></td>
                <td><?=$pro->stock;?></td>
                <td>
                    <a class="icon-btn" href="<?=base_url?>producto/editar&id=<?=$pro->id?>"><span
                            class="icon-edit"></span></a>
                    <a class="icon-btn" href="<?=base_url?>producto/eliminar&id=<?=$pro->id?>"> <span
                            class=" icon-trash"></span></a>
                </td>
            </tr>

            <?php endwhile; ?>
        </tbody>
    </table>
</div>