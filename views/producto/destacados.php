<div class=" slider">
    <ul>
        <li><img class="img-slider" src="<?=base_url ?>img/img2.jpg" alt=""></li>
        <li><img class="img-slider" src="<?=base_url ?>img/Banner-home-tienda-online.jpg" alt=""></li>
        <li><img class="img-slider" src="<?=base_url ?>img/img1.jpg" alt=""></li>
        <li><img class="img-slider" src="<?=base_url ?>img/img3.jpg" alt=""></li>
        <li><img class="img-slider" src="<?=base_url ?>img/img6.gif" alt=""></li>

    </ul>
</div>
<div class="container-cards">
    <div class="cards">
        <a href="<?=base_url?>categoria/ver&id=<?=1?>">
            <div class="icom-cat">
                <img src="img/icom/hombre.png">
            </div>
            <div class="titulo-cards">
                <h1>Hombre</h1>
            </div>
        </a>

    </div>
    <div class="cards">
        <a href="<?=base_url?>categoria/ver&id=<?=2?>">
            <div class="icom-cat">
                <img src="img/icom/mujer.png">
            </div>
            <div class="titulo-cards">
                <h1>Mujer</h1>
            </div>
        </a>


    </div>
    <div class="cards">
        <a href="<?=base_url?>categoria/ver&id=<?=3?>">
            <div class="icom-cat">
                <img src="img/icom/informatica.png">
            </div>
            <div class="titulo-cards">
                <h1>Informática</h1>
            </div>
        </a>

    </div>
    <div class="cards">
        <a href="<?=base_url?>categoria/ver&id=<?=4?>">
            <div class="icom-cat">
                <img src="img/icom/electrónica.png">
            </div>
            <div class="titulo-cards">
                <h1>Electrónica</h1>
            </div>
        </a>

    </div>
    <div class="cards">
        <a href="<?=base_url?>categoria/ver&id=<?=5?>">
            <div class="icom-cat">
                <img src="img/icom/escolar.png">
            </div>
            <div class="titulo-cards">
                <h1>Material Escolar</h1>
            </div>
        </a>
    </div>
    <div class="cards">
        <a href="<?=base_url?>categoria/ver&id=<?=5?>">
            <div class="icom-cat">
                <img src="img/icom/alimentos.png">
            </div>
            <div class="titulo-cards">
                <h1>Alimentos-bebidas</h1>
            </div>
        </a>
    </div>
    <div class="cards">
        <a href="<?=base_url?>categoria/ver&id=<?=6?>">
            <div class="icom-cat">
                <img src="img/icom/hogar.png">
            </div>
            <div class="titulo-cards">
                <h1>Hogar-Cocina</h1>
            </div>
        </a>


    </div>
</div>
<div class="titulo-h1">
    <h1 class="titulo-left">Productos destacadas</h1>
</div>
<div class="container">

    <?php while($product = $productos ->fetch_object()): ?>
    <div class="row-mx">
        <a href="<?=base_url?>producto/ver&id=<?=$product->id?>">
            <div class="image">
                <img src="<?=base_url?>uploads/images/<?=$product->imagen?>">

            </div>

            <h3 class="Precio"><?=$product->precio ?> Bs</h3>
            <div class="produc-titulo">
                <p class="Titulo"><?=$product->nombre ?></p>
            </div>


        </a>
        <div class="btn-cat-0">

            <a href=" <?=base_url?>carrito/add&id=<?=$product->id?>"><button class="btn-comprar-0" type="submit"
                    name="btnaccion" value="agregar">Comprar ya</button></a>

        </div>

    </div>
    <?php endwhile;?>
</div>