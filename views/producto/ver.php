<?php if(isset($product)):?>

<div class="produc-container">
    <div class="produc-caja-m5 ">
        <div class="produc-galer-img">
            <div class="col-row-img">
                <div class="column">
                    <img class="demo-cursor" src="<?=base_url?>uploads/images/<?=$product->imagen?>" style="width:100%"
                        onclick="currentSlide(1)" alt="img1">
                </div>
                <div class="column">
                    <img class="demo-cursor" src="<?=base_url?>uploads/images/<?=$product->imagen1?>" style="width:100%"
                        onclick="currentSlide(2)" alt="img2">
                </div>
                <div class="column">
                    <img class="demo-cursor" src="<?=base_url?>uploads/images/<?=$product->imagen2?>" style="width:100%"
                        onclick="currentSlide(3)" alt="img3">
                </div>
            </div>

            <div class="mySlides">

                <img class="img-ver" src="<?=base_url?>uploads/images/<?=$product->imagen?>" style="width:100%">
            </div>

            <div class="mySlides">

                <img class="img-ver" src="<?=base_url?>uploads/images/<?=$product->imagen1?>" style="width:100%">
            </div>
            <div class="mySlides">

                <img class="img-ver" src="<?=base_url?>uploads/images/<?=$product->imagen2?>" style="width:100%">
            </div>
        </div>

    </div>





    <div class="produc-caja-m5 ">



        <div class="caja-md-2">
            <div class="col-md-2">
                <div class="box-nombre">
                    <h2 class="titulo-produc"><?=$product->nombre ?></h2>
                </div>

                <div class="box-flex-prod">
                    <div class="box-data-precio">
                        <h2 class="titulo-h2"><?=$product->precio ?> Bs</h2>
                    </div>
                    <div class="box-data-precio">

                        <h2><del>599.99 Bs</del></h2>
                    </div>
                </div>
                <div>
                    <p><?=$product->stock ?> unidades disponibles </p>

                </div>
                <div class="box-flex-prod">
                    <div class="box-data-precio">
                        <h2>Se Envia desde:</h2>
                    </div>
                    <div class="box-data-precio">

                        <p> Entre rios</p>
                    </div>
                </div>
                <div class="box-flex-prod">
                    <div class="box-data-precio">
                        <h2>Costo de Envio: </h2>
                    </div>
                    <div class="box-data-precio">

                        <p>5 Bs</p>
                    </div>
                </div>
                <p>Tiempo de Entrega: El artículo será enviado dentro de 20-40 minutos.</p>



                <div class="caja-envio-precio-mas">
                    <div class="caja-comprar">
                        <div class="btn-cat-0">

                            <a href=" <?=base_url?>carrito/add&id=<?=$product->id?>"><button class="btn-comprar-0"
                                    type="submit" name="btnaccion" value="agregar">Comprar ya</button></a>

                        </div>
                        <div class="btn-cat-1">

                            <a href=" <?=base_url?>carrito/add&id=<?=$product->id?>"><button class="btn-comprar-1"
                                    type="submit" name="btnaccion" value="agregar">Añadir a carrito</button></a>

                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
    <div class="produc-destacados">
        <h3>Otras recomendaciones </h3>
        <div class="row-mx">
            <a href="<?=base_url?>producto/ver&id=<?=$product->id?>">
                <div class="image">
                    <img src="<?=base_url?>uploads/images/<?=$product->imagen?>">

                </div>

                <h3 class="Precio"><?=$product->precio ?> Bs</h3>
                <div class="produc-titulo">
                    <p class="Titulo"><?=$product->nombre ?></p>
                </div>


            </a>
        </div>

    </div>

</div>
<div class="container-descrip-produc">
    <div class="produc-destacados">
        <h3>Otras recomendaciones </h3>

        <div class="row-mx">
            <a href="<?=base_url?>producto/ver&id=<?=$product->id?>">
                <div class="image">
                    <img src="<?=base_url?>uploads/images/<?=$product->imagen?>">

                </div>

                <h3 class="Precio"><?=$product->precio ?> Bs</h3>
                <div class="produc-titulo">
                    <p class="Titulo"><?=$product->nombre ?></p>
                </div>
            </a>

        </div>


    </div>
    <div class="box-dis-produc">
        <div class="btn-produc">
            <button class="tabbutton" type="btn-pro" onclick="openCity(event, 'descripcion')">DESCRIPCION</button>
            <button class="tabbutton" type="btn-pro" onclick="openCity(event, 'detalles')">DETALLES</button>
        </div>
        <div class="tabcontent" id="descripcion" style=" display: block; border:0; padding:0px;">
            <div class="descip-produc">
                <p><?=$product->descripcion ?></p>
            </div>
            <div class="mySlides1">

                <img class="img-ver" src="<?=base_url?>uploads/images/<?=$product->imagen?>" style="width:100%">
            </div>
            <div class="mySlides1">

                <img class="img-ver" src="<?=base_url?>uploads/images/<?=$product->imagen1?>" style="width:100%">
            </div>
            <div class="mySlides1">

                <img class="img-ver" src="<?=base_url?>uploads/images/<?=$product->imagen2?>" style="width:100%">
            </div>

        </div>
        <div class="tabcontent" id="detalles">
            <ul>
                <li>1</li>
                <li>2</li>
                <li>3</li>
                <li>4</li>
                <li>5</li>
            </ul>

        </div>
    </div>
</div>
<div class="titulo-h1">
    <h1 class="titulo-left">Algunos de Nuestros Productos</h1>
</div>
<div class="container">

    <?php $productos =Utils::showproducto();?>
    <?php while($product = $productos ->fetch_object()): ?>
    <div class="row-mx">
        <a href="<?=base_url?>producto/ver&id=<?=$product->id?>">
            <div class="image">
                <img src="<?=base_url?>uploads/images/<?=$product->imagen?>">

            </div>

            <h3 class="Precio"><?=$product->precio ?> Bs</h3>
            <div class="produc-titulo">
                <p class="Titulo"><?=$product->nombre ?></p>
            </div>
        </a>

    </div>
    <?php endwhile;?>
</div>
<?php else:?>
<h1> el producto no existe</h1>
<?php endif; ?>


<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("demo");
    var captionText = document.getElementById("caption");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
    captionText.innerHTML = dots[slideIndex - 1].alt;
}
</script>