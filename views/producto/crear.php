<?php if(isset($edit) && isset($pro) && is_object($pro)): ?>
<h1>Editar un Producto</h1>
<h2><?=$pro->nombre?></h2>
<?php $url_action=base_url."producto/save&id=".$pro->id;?>
<?php else: ?>
<h1>Crear un Producto</h1>

<?php $url_action=base_url."producto/save";?>
<?php endif;?>
<form action="<?=$url_action?>" method="post" enctype="multipart/form-data">
    <div class="container-card">

        <div class="card-box-1">
            <label>Nombre</label>
            <input type="text" name="nombre" value="<?=isset($pro) && is_object($pro) ?$pro->nombre: ''; ?>"
                placeholder="nombre" required>
            <label>Descripcion</label>
            <textarea name="descripcion" cols="30" rows="10"
                placeholder="descripcion"><?=isset($pro) && is_object($pro) ?$pro->descripcion: ''; ?></textarea>
            <div class="container-perfil">
                <div>
                    <!--img 0 -->
                    <label>Imagen</label>
                    <?php if(isset($pro) && is_object($pro) && !empty($pro->imagen)):?>
                    <img class="img-produc" src="<?=base_url?>uploads/images/<?=$pro->imagen?>" />
                    <?php endif;?>
                    <input type="file" name="imagen"></div>
                <div>
                    <!--img 1 -->
                    <label>Imagen1</label>
                    <?php if(isset($pro) && is_object($pro) && !empty($pro->imagen1)):?>
                    <img class="img-produc" src="<?=base_url?>uploads/images/<?=$pro->imagen1?>" />
                    <?php endif;?>
                    <input type="file" name="imagen1">
                </div>
                <div>
                    <!--img 2 -->
                    <label>Imagen2</label>
                    <?php if(isset($pro) && is_object($pro) && !empty($pro->imagen2)):?>
                    <img class="img-produc" src="<?=base_url?>uploads/images/<?=$pro->imagen2?>" />
                    <?php endif;?>
                    <input type="file" name="imagen2">
                </div>
            </div>
            <label for="">Características</label>
            <textarea name="" id="" cols="30" rows="10"></textarea>
            <label for="">Marca</label>
            <select name="marca" id=""></select>
            <label for="">Proveedor</label>
            <select name="proveedor" id=""></select>
        </div>
        <div class="card-box-2">

            <label>Referencia</label>
            <input type="text" name="referencia">
            <label>Precio</label>
            <input type="text" name="precio" value="<?=isset($pro) && is_object($pro) ?$pro->precio: ''; ?>"
                placeholder="precio" required>
            <label>Stock</label>
            <input type="number" name="stock" value="<?=isset($pro) && is_object($pro) ?$pro->stock: ''; ?>"
                placeholder="stock" required>
            <label>Categoria</label>
            <?php $categorias =Utils::showCategorias();?>
            <select name="categoria">
                <?php while ($cat = $categorias->fetch_object()):?>
                <option value="<?=$cat->id?>"
                    <?=isset($pro) && is_object($pro) && $cat->id ==$pro->categoria_id ? 'selected':''; ?>>
                    <?=$cat->id?>
                    <?=$cat->nombre?>

                </option>

                <?php endwhile;?>
            </select>
            <!--sub categorias del producto-->
            <label>Sub-Categoria</label>
            <?php  $subcategorias = Utils::showSubcategorias(); ?>


            <select name="subcategoria">
                <?php while ($subcat = $subcategorias->fetch_object()):?>
                <option value="<?=$subcat->id?>"
                    <?=isset($pro) && is_object($pro) && $subcat->id ==$pro->subcategoria_id ? 'selected':''; ?>>
                    <?=$subcat->categoria_id?>
                    <?=$subcat->nombre?>

                </option>

                <?php endwhile; ?>
            </select>

        </div>


    </div>
    <input type="submit" value="Guardar">
</form>