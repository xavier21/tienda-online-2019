<div class="container-card">
    <div class=admin-h2>
        <h2>Gestionar Categorias</h2>
    </div>
    <div class="btn-crear">
        <a href="<?=base_url?>categoria/crear"><span class="icon-edit"></span></a>
    </div>
</div>
<div class="table-admi">
    <table border="1">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
            </tr>
        </thead>
        <tbody>
            <?php while($cat = $categorias->fetch_object()):?>
            <tr>
                <td><?=$cat->id;?></td>
                <td><?=$cat->nombre;?></td>
            </tr>

            <?php endwhile; ?>
        </tbody>
    </table>
</div>