

<?php if(isset($categoria)):?>
    <h1><?=$categoria->nombre ?></h1>
    <?php if($productos->num_rows == 0): ?>
        <p> No Hay Productos Para Mostrar</p>
    <?php else:?>
    <div class="container">
    <?php while($product = $productos->fetch_object()): ?>
    <div class="row-mx">
        <a href="<?=base_url?>producto/ver&id=<?=$product->id?>">
            <div class="image">
                <img src="<?=base_url?>uploads/images/<?=$product->imagen?>">

            </div>
         
           <div class="produc-titulo">
           <h3 class="Precio"><?=$product->precio ?> Bs</h3>
           </div>
            <div class="produc-titulo">
            <p class="Titulo"><?=$product->nombre ?></p>
            </div>
         

        </a>
        <div class="caja-comprar">

            <div class="btn-cat-1">
               
                <a href=" <?=base_url?>carrito/add&id=<?=$product->id?>"><button class="btn-comprar-1" type="submit" name="btnaccion" value="agregar">Comprar</button></a>
            
            </div>


        </div>



    </div>


    <?php endwhile;?>
</div>


    <?php endif;?>
<?php else:?>
    <h1> la categoria no existe</h1>
<?php endif; ?>