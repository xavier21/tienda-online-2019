<!DOCTYPE html>
<html lang="es">

<head>
    <?php
    //problema "Headers already sent" de PHP
    ob_start();
    ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AmazonasExpress</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url ?>css/descripcion.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url ?>css/estilos.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url ?>css/menu.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url ?>icom/fontello/css/fontello.css">
    <!--link de footer -->
    <link rel="stylesheet" type="text/css" href="<?=base_url ?>css/pie-de-pagina.css">
    <!--link de form registro -->
    <link rel="stylesheet" type="text/css" href="<?=base_url ?>css/formulario.css">
    <!--link de gestionar categorias-->
    <link rel="stylesheet" type="text/css" href="<?=base_url ?>css/gestionarcategorias.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!--link de  h4-->
    <link href="https://fonts.googleapis.com/css?family=Arvo&display=swap" rel="stylesheet">
    <!--link de  h2-->
    <link href="https://fonts.googleapis.com/css?family=Domine&display=swap" rel="stylesheet">
    <!--link de  h3-->
    <link href="https://fonts.googleapis.com/css?family=Lobster&display=swap" rel="stylesheet">

    <script type="text/javascript" src="../menu.js"></script>
    <!--link de   menu font-family: 'Crimson Pro', serif-->
    <link href="https://fonts.googleapis.com/css?family=Crimson+Pro&display=swap" rel="stylesheet">

    <!--link de   menu font-family: 'Sanchez', serif-->
    <link href="https://fonts.googleapis.com/css?family=Sanchez&display=swap" rel="stylesheet">

    <!--link de   menu f   font-family: 'Arvo', serif-->
    <link href="https://fonts.googleapis.com/css?family=Arvo&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=News+Cycle&display=swap" rel="stylesheet">
    <script type="text/javascript" src="menu.js"></script>
    <script type="text/javascript" src="menuactivate.js"></script>
  
</head>

<body>
    <div class="container-oficialmenu">
        <div class="container-menu1">
            <div class="logo-menu">
                <a href="<?=base_url?>">
                    <img src="img/logo.png" alt="">
                </a>

            </div>
            <div class="search">
                <input type="search" id="search" placeholder="Buscar en AmaxExpress">
                <button type="submit">Buscar</button>
            </div>
            <div class="cesta-logo">


                <div class="dropdown">
                    <div class="menu-btn"> <a class="dropbtn-btn" href="#">Mi cuenta</a></div>


                    <div class="dropdown-content">
                        <?php if(!isset($_SESSION['identity'])):?>
                        <a href="<?=base_url?>usuario/registro">Iniciar Seccion</a>
                        <?php else: ?>
                        <a href="<?=base_url?>usuario/perfil"> <?=$_SESSION['identity']->nombres?>
                            <?=$_SESSION['identity']->apellidos?></a>
                        <?php endif; ?>
                        <?php if(isset($_SESSION['admin'])):?>
                        <a href="<?=base_url?>categoria/index">Gestionar Categorias</a>
                        <a href="<?=base_url?>subcategoria/index">Gestionar Subcategorias</a>
                        <a href="<?=base_url?>producto/gestion">Gestionar Productos</a>
                        <a href="<?=base_url?>pedido/gestion">Gestionar Pedidos</a>
                        <?php endif; ?>

                        <?php if(isset($_SESSION['identity'])):?>
                        <a href="<?=base_url?>pedido/mis_pedidos">Mis Pedidos</a>
                        <a href="<?=base_url?>usuario/logout">Salir</a>
                        <?php else:?>
                        <a href="<?=base_url?>usuario/registro">Restrate Aqui?</a>

                        <?php endif; ?>

                    </div>

                </div>
                <?php $stats =Utils::statsCarrito(); ?>
                <div class="dropdown">
                    <div class="menu-btn"> <a class="dropbtn-btn" href="<?=base_url?>carrito/index"><span
                                class="icon-basket">( <?=$stats['count']?> )</span></a></div>

                    <div class="dropdown-content">
                        <a href="#">Precio Total <?=$stats['total']?> Bs</a>

                    </div>
                </div>

            </div>

        </div>


        <div class="topnav" id="myTopnav">
            <!--MENU -->
            <?php $categorias = Utils::showCategorias(); ?>
            <?php //print_r($categorias);?>
            <a href="<?=base_url?>">Inicio</a>

            <div class="dropdown">
                <button class="dropbtn">
                    Moda hombres
                </button>
                <div class="dropdown-content">
                    <?php $subcategorias = Utils::showSubcategorias(); ?>
                    <a href="<?=base_url?>categoria/ver&id=<?=1?>">Todos los Productos</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=3?>">Chaquetas y abrigos</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=30?>">Pantalones Rasgados</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=33?>">Deportivos</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=1?>">Camisas</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=5?>">Zapatos</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=32?>">Ropa Interior</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=31?>">Calsetines</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">
                    Moda Mujeres </button>
                <div class="dropdown-content">
                    <?php $subcategorias = Utils::showSubcategorias(); ?>
                    <a href="<?=base_url?>categoria/ver&id=<?=2?>">Todos los Productos</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=39?>">Chaquetas y abrigos</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=34?>">Pantalones Cortos y Shorts</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=38?>">Blusas y camisas</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=36?>">leggings</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=2?>">Vestidos</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=35?>">Faldas</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=40?>">Jeans</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=37?>">Deporte</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">
                    Informatica</button>
                <div class="dropdown-content">
                    <?php $subcategorias = Utils::showSubcategorias(); ?>
                    <a href="<?=base_url?>categoria/ver&id=<?=3?>">Todos los Productos</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=4?>">Ratones</a>
                    <!-- <a href="<?=base_url?>subcategoria/ver&id=<?=0?>">Teclados</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=0?>">Prosesadores</a>-->
                    <a href="<?=base_url?>subcategoria/ver&id=<?=8?>">Placas Base</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=9?>">Tarjetas Graficas</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=10?>">Portatiles</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=11?>">Tables</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=12?>">Unidades Flash USB</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=12?>">Tarjetas de Menoria</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=14?>">Discos Duros HDD SDD</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">
                    Electronica</button>
                <div class="dropdown-content">

                    <?php $subcategorias = Utils::showSubcategorias(); ?>
                    <a href="<?=base_url?>categoria/ver&id=<?=4?>">Todos los Productos</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=15?>">Baterias</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=16?>">Cables y Adactadores</a>
                    <!--  <a href="<?=base_url?>subcategoria/ver&id=<?=0?>">Cargadores</a>-->
                    <a href="<?=base_url?>subcategoria/ver&id=<?=17?>">Bolsas y Fundas</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=18?>">Televisores</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=19?>">Proyectores</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=20?>">Altavoces</a>
                    <!-- <a href="<?=base_url?>subcategoria/ver&id=<?=0?>">Reproductores MP3</a>-->
                    <a href="<?=base_url?>subcategoria/ver&id=<?=7?>">Auriculares y Cascos</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=6?>">Camaras</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">
                    Material Escolar</button>
                <div class="dropdown-content">
                    <a href="">Todos los Productos</a>
                    <a href="#">Mochilas y Estuches</a>
                    <a href="#">Cuadernos</a>
                    <a href="#">Pinrulas Y Lapizes</a>
                    <a href="#">Boligrafos</a>
                    <a href="#">Libros</a>
                    <a href="#">Otros</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">
                    Alimentos y
                    Bebidas </button>
                <div class="dropdown-content">
                    <?php $subcategorias = Utils::showSubcategorias(); ?>
                    <a href="<?=base_url?>categoria/ver&id=<?=5?>">Todos los Productos</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=21?>">Lactios</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=22?>">Reposteria</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=23?>">Jugos y Refresco</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=24?>">Restaurantes</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=25?>">Bebidas Alcoholicas</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">
                    Hogar y Cocina</button>
                <div class="dropdown-content">

                    <?php $subcategorias = Utils::showSubcategorias(); ?>
                    <a href="<?=base_url?>categoria/ver&id=<?=6?>">Todos los Productos</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=26?>">Utensilios para el Hogar</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=27?>">Vasos,Copas y Tasas</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=28?>">Ropa de Cama</a>
                    <a href="<?=base_url?>subcategoria/ver&id=<?=29?>">Relojes de Pared</a>
                </div>
            </div>
            <a href="<?=base_url?>">Ayuda</a>


            <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>
        </div>






    </div>
    <!-- menu botton-->
    <div class="menu-bottom">

        <div class="btn-box">
            <a href="<?=base_url?>">
                <div class="icom"><span class="icon-home-outline"></span></div>
                <div class="name-btn"> Inicio</div>
            </a>

        </div>
        <div class="btn-box">
            <a href="#">
                <div class="icom"><span class="icon-th-list-outline"></span></div>
                <div class="name-btn">Categorias</div>
            </a>
        </div>
        <div class="btn-box">
            <a class="dropbtn-btn" href="<?=base_url?>carrito/index">
                <div class="icom">
                    <span class="icon-basket"></span>
                </div>
                <div class="name-btn">Cesta (<?=$stats['count']?>)</div>
            </a>
        </div>
        <div class="btn-box">
            <?php if(!isset($_SESSION['identity'])):?>
            <a href="<?=base_url?>usuario/registro">
                <div class="icom"><span class="icon-user"></span></div>
                <div class="name-btn"> Mi Cuenta</div>
            </a>
            <?php else: ?>
            <a href="<?=base_url?>usuario/perfil">
                <div class="icom"><span class="icon-user"></span></div>
                <div class="name-btn"> Mi Cuenta</div>
            </a>
            <?php endif; ?>
        </div>



    </div>