<?php if(isset($gestion)): ?>
<h1>Gestionar pedidos</h1>
<?php else: ?>
<h1>Mis Pedidos</h1>
<?php endif; ?>
<div class="table-admi">
    <table>
        <thead>
            <tr>
                <th>N° Pedido</th>
                <th>Fecha</th>
                <th>Precio total</th>

                <th>Estado</th>

            </tr>

        </thead>
        <tbody>
            <?php
      while($ped = $pedidos->fetch_object()):
    ?>
            <tr>
                <td><a href="<?=base_url?>pedido/detalle&id=<?=$ped->id?>"><?=$ped->id?></a></td>
                <td><?=$ped->fecha?></td>
                <td><?=$ped->coste?> Bs</td>

                <td><?= Utils::showStatus($ped->estado)?></td>
            </tr>


            <?php endwhile;?>
        </tbody>
    </table>
</div>