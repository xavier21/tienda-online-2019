<?php if(isset($_SESSION['pedido']) && $_SESSION['pedido'] == 'complete'):?>
<h1>Tu Pedido se ha confirmado</h1>
<p> tu pedido ha sido guardado con exito y sera enviado una ves que llege el envio pagaras la cantidad total del
    producto</p>

<br />
<?php if(isset($pedido)): ?>
<h3>Datos del Pedido</h3>
Numero de Pedido:<?=$pedido->id ?> <br />
Total a Pagar:<?=$pedido->coste ?> Bs <br />
Productos:

<table>
    <thead>
        <tr>
            <th>Imagen</th>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Unidades</th>

        </tr>
    </thead>
    <tbody>
        <?php while($producto =$productos->fetch_object()):?>
        <tr>
            <td><img class="img-carrito" src="<?=base_url?>uploads/images/<?=$producto->imagen?>"></td>
            <td><?=$producto->nombre?></td>
            <td><?=$producto->precio?> Bs</td>
            <td> <?=$producto->unidades?> Uniadades</td>

        </tr>
        <?php endwhile; ?>
    </tbody>
</table>


<?php endif;?>


<?php elseif(isset($_SESSION['pedido']) && $_SESSION['pedido'] != 'complete'):?>
<h1>Tu Pedido no ha podido confirmarse</h1>
<?php endif;?>