            <?php if(isset($_SESSION['identity'])): ?>
            <!-- <h1> Hacer Pedido</h1>-->
            <div>
                <h1 class="titulo-left">Información de envío</h1>
            </div>
            <div class="container-card">

                <div class="Direcc-Envio">

                    <form action="<?=base_url.'pedido/add'?>" method="POST">
                        <div class="container-card-2">
                            <div class="form-txt-1"><label>Nombre completo</label>
                                <input type="text" name="nombrecompleto" placeholder="xavier castillo rosa"></div>
                            <div class="form-txt-1"><label>Celular</label>
                                <input type="text" name="celular" placeholder="73433739"></div>
                        </div>
                        <div class="container-card-2">
                            <div class="form-txt-1"><label>Identidad</label>
                                <input type="text" name="identidad" placeholder="13577413-CBBA"></div>
                            <div class="form-txt-selec"><label>Localidad</label>
                                <select name="localidad">
                                    <option value="Entre Rios">Entre Rios</option>
                                    <option value="Entre Rios">Rio blanco</option>
                                    <option value="Entre Rios">Bulo Bulo</option>
                                    <option value="Manco Kapac">Manco Kapac</option>
                                    <option value="Cruce Andino">Cruce Andino</option>
                                    <option value="Gualberto Villarroel">Gualberto Villarroel</option>
                                    <option value="Entre Rios">Valle Sacta</option>
                                    <option value="Entre Rios">Ivirgarzama</option>


                                    </option>
                                </select></div>
                        </div>
                        <div class="container-card-2">
                            <div class="form-txt-1"><label>Domicilio</label>
                                <input type="text" name="domicilio" placeholder="Barrio 14 de septiembre"></div>
                            <div class="form-txt-1"><label>Direccion</label>
                                <input type="text" name="direccion" placeholder="Calle:10 de Noviembre"></div>

                        </div>
                        <input type="submit" value="Confirmar Pedido">
                    </form>
                    <div class="href-verproduc">
                        <a href="<?=base_url?>carrito/index"> Ver los Productos y el Precio del Pedido</a>
                    </div>
                </div>

                <div class="card-box-2">
                    <div class="box-pago">
                        <div class="resumen-pedido">
                            <h2> Resumen del pedido</h2>
                        </div>
                        <hr>
                        <div class="box-precio">
                            <div class="box-1">
                                <h3>Total</h3>
                            </div>
                            <div class="box-2"><?php $stats =Utils::statsCarrito(); ?>
                                <h4> <?=$stats['total']?> Bs</h4>
                            </div>
                        </div>
                    </div>
                    <div class="box-Política">
                        <div>
                            <p class="text-left">Política de seguridad (editar con el módulo Información de seguridad y
                                confianza
                                para el
                                cliente) Política de seguridad (editar con el módulo Información de seguridad y
                                confianza para el cliente)</p>
                        </div>
                        <hr>
                        <div>
                            <p class="text-left">Política de envío (editar con el módulo Información de seguridad y
                                confianza para
                                el
                                cliente) Política de envío (editar con el módulo Información de seguridad y confianza
                                para el cliente)</p>
                        </div>
                        <hr>
                        <div>
                            <p class="text-left"> Política de devolución (editar con el módulo Información de seguridad
                                y confianza
                                para
                                el cliente)</p>
                        </div>

                    </div>

                </div>

            </div>


            <?php else: ?>
            <h1>Necesitas estar identificado</h1>
            <p>Necesitas estar logueado en la web ára poder realizar el pedido.</p>

            <?php endif ?>