<h1 class="h1-left">Detalle del Pedido</h1>
<div class="container-detalle">
    <?php if(isset($pedido)): ?>

    <?php if(isset($_SESSION['admin'])): ?>

    <div class="estado-pedido">
        <h3>Canbiar estado del pedido</h3>
        <form action="<?=base_url?>pedido/estado" method="POST">
            <input type="hidden" value="<?=$pedido->id?>" name="pedido_id" />
            <select name="estado">
                <option value="confirm" <?=$pedido->estado == "confirm" ?'selected' : ''; ?>>Pendiente</option>
                <option value="preparation" <?=$pedido->estado == "preparation" ?'selected' : ''; ?>>En preparacion
                </option>
                <option value="ready" <?=$pedido->estado == "raedy" ?'selected' : ''; ?>>Preparado para enviar</option>
                <option value="sended" <?=$pedido->estado == "sended" ?'selected' : ''; ?>>Enviado</option>
            </select>
            <input type="submit" value="Cambiar estado" />

        </form>
    </div>

    <?php endif;?>
    <div class="txt-envio">
        <h3>Referencia de pedido BNXOTONDT - efectuado el <?=$pedido->fecha ?> </h3>
    </div>
    <div class="container-dat-envio">
        <div class="info-envio">
            <ul class="data-envio">
                <li>N° Pedido:</li>
                <li>Estado:</li>

            </ul>
        </div>
        <div class="info-envio">
            <ul class="data-envio">
                <li>MK21E19-<?=$pedido->id ?></li>
                <li><?= Utils::showStatus($pedido->estado)?></li>

            </ul>
        </div>

    </div>
    <div class="txt-envio">
        <h3>Dirección de entrega Mi Dirección</h3>
    </div>
    <div class="container-dat-envio">

        <div class="info-envio">
            <ul class="data-envio">
                <li>Nombre completo:</li>
                <li>Identidad:</li>
                <li>Celular:</li>
                <li>Localidad:</li>
                <li>Domicilio:</li>
                <li>Direccion:</li>
            </ul>
        </div>
        <div class="">
            <ul class="data-envio">
                <li><?=$pedido->nombrecompleto ?></li>
                <li><?=$pedido->identidad ?></li>
                <li><?=$pedido->celular ?></li>
                <li><?=$pedido->localidad ?></li>
                <li><?=$pedido->domicilio ?></li>
                <li><?=$pedido->direccion ?></li>
            </ul>
        </div>
    </div>

    <div class="txt-envio">
        <h3>Detalles del Articulos</h3>
    </div>

    <div class="container-dat-envio">
    
        <div class="card-box-produc">
            <?php while($producto =$productos->fetch_object()):?>
            <div class="container-produc-card">
                <div class="data-info-produc"><img class="img-carrito"
                        src="<?=base_url?>uploads/images/<?=$producto->imagen?>"></div>
                <div class="data-info-produc">
                    <p class="text-producto"><?=$producto->nombre?></p>
                </div>
                <div class="data-info-produc">
                    <p class="text-producto"><?=$producto->unidades?> Unidades</p>
                </div>
                <div class="data-info-produc">
                    <p class="text-producto"><?=$producto->precio?> Bs</p>
                </div>

            </div>
            <hr>
            <?php endwhile; ?>

        </div>

    </div>
    <div class="container-dat-envio">
        <table>
            <tr>
                <td>
                    <h4> Total a Pagar:</h4>
                </td>
                <td> <?=$pedido->coste ?> Bs</td>
            </tr>

        </table>

    </div>







    <?php endif;?>
</div>