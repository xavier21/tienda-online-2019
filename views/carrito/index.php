<?php if (isset($_SESSION['carrito']) && count($_SESSION['carrito']) >= 1): ?>
<div class="titulo-car">
    <h2>Carrito de Compras</h2>
</div>
<div class="container-card">
    <div class="card-box-1">
        <div class="card-box-produc">
            <?php
            foreach($carrito as $indice => $elemento):
                    $producto = $elemento['producto'];
             ?>
            <div class="container-produc-card">
                <div class="data-info-produc"><img class="img-carrito"
                        src="<?=base_url?>uploads/images/<?=$producto->imagen?>"></div>
                <div class="data-info-produc">
                    <p class="text-producto"><a
                            href="<?=base_url?>producto/ver&id=<?=$producto->id ?>"><?=$producto->nombre?></a></p>
                    <p class="text-producto"><?=$producto->precio?> Bs</p>
                </div>
                <div class="data-producto">

                    <div class="data-info"> <a href="<?=base_url?>carrito/down&index=<?=$indice?>"><span
                                class="icon-minus"></span></a>
                        <?=$elemento['unidades']?>
                        <a href="<?=base_url?>carrito/up&index=<?=$indice?>"><span class="icon-plus"></span></a>
                    </div>
                    <div class="data-info"><?=$producto->precio?> Bs</div>
                    <div class="data-info"><a href="<?=base_url?>carrito/delete&index=<?=$indice?>"> <span
                                class=" icon-trash"></span></a>
                    </div>
                </div>

            </div>
            <hr>

            <?php endforeach;?>
        </div>
        <div class="btn-pedido">

            <div class="box-button">
                <a class="a-btn-v" href="<?=base_url?>carrito/delete_all">Vaciar Carrito</a>
            </div>
            <div class="box-button">
                <a class="a-btn" href="<?=base_url?>pedido/hacer"><button class="button-pedido">Ralizar
                        Pedido</button></a>
            </div>
        </div>

    </div>
    <div class="card-box-2">
        <div class="box-pago">
            <div class="resumen-pedido">
                <h2> Resumen del pedido</h2>
            </div>
            <hr>
            <?php $stats =Utils::statsCarrito(); ?>
            <div class="box-precio">
                <div class="box-1">
                    <h3>(<?=$stats['count']?>) artículos</h3>
                </div>
                <div class="box-2">
                    <h4> <?=$stats['total']?> Bs</h4>
                </div>

            </div>
            <div class="box-precio">
                <div class="box-1">
                    <h3>Transporte</h3>
                </div>
                <div class="box-2">
                    <h4> 000 Bs</h4>
                </div>
            </div>
            <hr>
            <div class="box-precio">
                <div class="box-1">
                    <h3>Total</h3>
                </div>
                <div class="box-2">
                    <h4> <?=$stats['total']?> Bs</h4>
                </div>
            </div>
        </div>

        <div class="box-Política">
            <div>
                <p class="text-left">Política de seguridad (editar con el módulo Información de seguridad y confianza
                    para el
                    cliente) Política de seguridad (editar con el módulo Información de seguridad y
                    confianza para el cliente)</p>
            </div>
            <hr>
            <div>
                <p class="text-left">Política de envío (editar con el módulo Información de seguridad y confianza para
                    el
                    cliente) Política de envío (editar con el módulo Información de seguridad y confianza
                    para el cliente)</p>
            </div>
            <hr>
            <div>
                <p class="text-left"> Política de devolución (editar con el módulo Información de seguridad y confianza
                    para
                    el cliente)</p>
            </div>

        </div>
    </div>
</div>
<div>
    <?php else: ?>

    <div class="imgcard-vacio">
        <img src="<?=base_url ?>img/carrito-vacio.jpg">
    </div>
    <h4>Tu Cesta está vacía</h4>
    <p>Descubre increíbles ofertas o <a href="<?=base_url?>/usuario/registro">inicia sesión</a> para ver los artículos
        de tu Cesta.</p>

    <?php endif; ?>
</div>