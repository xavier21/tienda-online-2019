<?php
class Producto{
    private $id;
    private $categoria_id;
    private $subcategoria_id;
    private $nombre;
    private $descripcion;
    private $precio;
    private $stock;
    private $oferta;
    private $fecha;
    private $imagen;
    private $imagen1;
    private $imagen2;
    private $db;

    public function __construct() {
        $this->db = Database::connect();
    }
    function getId(){
        return $this->id;
    }
    function getCategoria_id(){
        return $this->categoria_id;
    }
    function getSubcategoria_id(){
        return $this->subcategoria_id;
    }
    function getNombre(){
        return $this->nombre;
    }
    function getDescripcion(){
        return $this->descripcion;
    }
    function getPrecio(){
        return $this->precio;
    }
    function getStock(){
        return $this->stock;
    }
    function get0ferta(){
        return $this->oferta;
    }
    function getFecha(){
        return $this->fecha;
    }
    function getImagen(){
        return $this->imagen;
    }
    function getImagen1(){
        return $this->imagen1;
    }
    function getImagen2(){
        return $this->imagen2;
    }
    function setId($id){
        $this->id=$id;
    }
    function setCategoria_id($categoria_id){
        $this->categoria_id=$categoria_id;
    }
    function setSubcategoria_id($subcategoria_id){
        $this->subcategoria_id=$subcategoria_id;
    }
    function setNombre($nombre){
        $this->nombre= $this->db->real_escape_string($nombre);
    }
    function setDescripcion($descripcion){
        $this->descripcion=  $this->db->real_escape_string($descripcion);
    }
    function setPrecio($precio){
        $this->precio=  $this->db->real_escape_string($precio);
    }
    function setStock($stock){
        $this->stock=  $this->db->real_escape_string($stock);
    }
    function setOferta($oferta){
        $this->oferta=  $this->db->real_escape_string($oferta);
    }
    function setFecha($fecha){
        $this->fecha=$fecha;
    }
    function setImagen($imagen){
        $this->imagen=$imagen;
    }
    function setImagen1($imagen1){
        $this->imagen1=$imagen1;
    }
    function setImagen2($imagen2){
        $this->imagen2=$imagen2;
    }
    public function getALL(){
        $productos =$this->db->query("SELECT * FROM productos ORDER BY id DESC");
        return $productos;
    }
     //sacar productos segun su categoria
     public function getALLCategory(){
         $sql="SELECT p.*, c.nombre AS 'catnombre' FROM productos p "
         ."INNER JOIN categorias c ON c.id = p.categoria_id "
         ."WHERE p.categoria_id ={$this->getCategoria_id()} "
         ."ORDER BY id DESC";
        $productos =$this->db->query($sql);
        return $productos;
    }
    //sacar productos segun su subcategoria
    public function getALLSubcategory(){
            $sql="SELECT p.*, c.nombre AS 'catnombre' FROM productos p "
            ."INNER JOIN subcategorias c ON c.id = p.subcategoria_id "
            ."WHERE p.subcategoria_id ={$this->getSubcategoria_id()} "
            ."ORDER BY id DESC";
           $productos =$this->db->query($sql);
           return $productos;
       }

    //mostrar productos en index
    public function getRandom($limit){
        $productos=$this->db->query("SELECT * FROM productos ORDER BY RAND() LIMIT $limit");
        return $productos;


    }
    /* mostrar productos para mujere
    public function getRandom_2($limite){
        $productosy=$this->db->query("SELECT * FROM productos WHERE subcategoria_id ='2' ORDER BY RAND() LIMIT $limite");
        return $productosy;

    }
    */



//Editar un producto
    public function getOne(){
        $producto =$this->db->query("SELECT * FROM productos WHERE id={$this->getId()}");
        return $producto->fetch_object();
    }
//guarda un producto
    public function save(){
        $sql= "INSERT INTO productos VALUES ( NULL,'{$this->getCategoria_id()}','{$this->getSubcategoria_id()}','{$this->getNombre()}','{$this->getDescripcion()}',{$this->getPrecio()},{$this->getStock()},'NULL',CURDATE(),'{$this->getImagen()}','{$this->getImagen1()}','{$this->getImagen2()}');";
        $save = $this->db->query($sql);
        // var_dump($sql); 
         //echo $this->db->error;
         // die;
           
            $result = false;
            if($save){
                $result = true;
            }
            return $result;
    
    }
// Guardar producto Editado
    public function edit(){
        $sql= "UPDATE productos SET categoria_id='{$this->getCategoria_id()}',subcategoria_id='{$this->getSubcategoria_id()}', nombre='{$this->getNombre()}',descripcion='{$this->getDescripcion()}',precio={$this->getPrecio()},stock={$this->getStock()} ";
        if($this->getImagen() && $this->getImagen1() && $this->getImagen2() != null){

             $sql .= ", imagen='{$this->getImagen()}', imagen1='{$this->getImagen1()}' , imagen2='{$this->getImagen2()}'";

        }
        $sql.=" WHERE id={$this->id};";

        $save = $this->db->query($sql);
           
            $result = false;
            if($save){
                $result = true;
            }
            return $result;
    
    }
//Eliminar un producto
    public function delete(){
        $sql ="DELETE FROM productos WHERE id=($this->id)";
        $delete=$this->db->query($sql);

        $result = false;
        if($delete){
            $result = true;
        }
        return $result;

    }

}