<?php
class Usuario{
    private $id;
    private $nombres;
    private $apellidos;
    private $email;
    private $celulaIdentidad;
    private $celular;
    private $password;
    private $rol;
    private $db;

    public function __construct() {
        $this->db = Database::connect();
    }
    function getId(){
        return $this->id;
    }
    function getNombres(){
        return $this->nombres;
    }
    function getApellidos(){
        return $this->apellidos;
    }
    function getEmail(){
        return $this->email;
    }
    function getCelulaIdentidad(){
        return $this->celulaIdentidad;
    }
    function getCelular(){
        return $this->celular;
    }
    function getPassword(){
        return  password_hash($this->db->real_escape_string($this->password),PASSWORD_BCRYPT,['cost'=>4]);
        //return  $this->password;
    }
    function getRol(){
        return $this->rol;
    }

    function setId($id){
       $this->id =$id;
    }
    function setNombres($nombres){
        $this->nombres = $this->db->real_escape_string($nombres);
    }
    function setApellidos($apellidos){
        $this->apellidos = $this->db->real_escape_string($apellidos);
    }
    function setEmail($email){
        $this->email = $this->db->real_escape_string($email);
    }
    function setCelulaIdentidad($celulaIdentidad){
        $this->celulaIdentidad = $this->db->real_escape_string($celulaIdentidad);
    }
    function setCelular($celular){
        $this->celular = $this->db->real_escape_string($celular);
    }
    function setPassword($password){
       // $this->password = password_hash($this->db->real_escape_string($password),PASSWORD_BCRYPT,['cost'=>4]);
       $this->password = $password;
    }
    function setRol($rol){
        $this->rol =$rol;
    }
    public function save(){
    $sql= "INSERT INTO usuarios VALUES ( NULL ,'{$this->getNombres()}','{$this->getApellidos()}','{$this->getEmail()}'
    ,'{$this->getCelulaIdentidad()}','{$this->getCelular()}','{$this->getPassword()}','user');";
        $save = $this->db->query($sql);
      // var_dump ($sql);
      // $this->db ;
      // die;
        $result = false;
        if($save){
            $result = true;
        }
        return $result;
    }
    public function login(){
        $result=false;
        $email = $this->email;
        $password = $this->password;
        //Comprobar si existe el usuario
        $sql = "SELECT * FROM usuarios WHERE email ='$email'";
        $login=$this->db->query($sql);

        if($login && $login->num_rows == 1){
            $usuario = $login->fetch_object();
            //var_dump($usuario);
            
            //Verificar la Comtraseña
            $verify = password_verify($password,$usuario->password);
            if($verify){
                $result=$usuario;

            }

        }
    return $result;
    }
} 
?>