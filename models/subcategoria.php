<?php
class Subcategoria{
    private $id;
    private $categoria_id;
    private $nombre;
    private $db;

    public function __construct() {
        $this->db = Database::connect();
    }
    function getId() {
		return $this->id;
    }
    function getCategoria_Id() {
		return $this->categoria_id;
	}
    function getNombre() {
		return $this->nombre;
	}


	function setId($id) {
		$this->id = $id;
    }
    function setCategoria_Id($categoria_id) {
		$this->categoria_id = $categoria_id;
    }
    
	function setNombre($nombre) {
        $this->nombre = $this->db->real_escape_string($nombre);
    }

    public function getALL(){
        $subcategorias =$this->db->query( "SELECT * FROM subcategorias;");
        return $subcategorias;
    }
    //ojo
    /*saca el subcategoria de categoria para el menu
     public function getALLSubcat(){
      $sql="SELECT s.*, c.nombre FROM subcategoria s "
      ."INNER JOIN categorias c ON c.id = s.categoria_id "
      ."WHERE s.categoria_id ={$this->getcategoria_id()} "
      ."ORDER BY id DESC";
     $subcategoria =$this->db->query($sql);
     return $subcategoria;
 }*/
//codigo para sacar el nombre de la sub categoria
    public function getOne(){
      $subcategoria =$this->db->query("SELECT * FROM subcategorias  WHERE id={$this->getId()}");
      
      return $subcategoria->fetch_object();
      
  }
//guarda la subcategoria
    public function save(){
        $sql= "INSERT INTO subcategorias VALUES ( NULL ,'{$this->getCategoria_Id()}','{$this->getNombre()}');";
            $save = $this->db->query($sql);
           
            $result = false;
            if($save){
                $result = true;
            }
            return $result;
        }
}