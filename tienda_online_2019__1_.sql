-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 03-12-2019 a las 21:47:37
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda_online_2019`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(255) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`) VALUES
(1, 'Moda Hombre'),
(2, 'Moda Mujer'),
(3, 'Informatica'),
(4, 'Electronica'),
(5, 'Alimentos y Bebidas'),
(6, 'Hogar y Cocina');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lineas_pedidos`
--

CREATE TABLE `lineas_pedidos` (
  `id` int(255) NOT NULL,
  `pedido_id` int(255) NOT NULL,
  `producto_id` int(255) NOT NULL,
  `unidades` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lineas_pedidos`
--

INSERT INTO `lineas_pedidos` (`id`, `pedido_id`, `producto_id`, `unidades`) VALUES
(1, 1, 1, 1),
(5, 4, 7, 1),
(6, 5, 2, 1),
(7, 6, 2, 3),
(8, 6, 8, 1),
(9, 7, 6, 4),
(10, 7, 7, 1),
(11, 7, 3, 1),
(12, 8, 6, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(255) NOT NULL,
  `usuario_id` int(255) NOT NULL,
  `nombrecompleto` varchar(100) NOT NULL,
  `identidad` int(10) NOT NULL,
  `celular` int(10) NOT NULL,
  `localidad` varchar(100) NOT NULL,
  `domicilio` varchar(100) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `coste` float(200,2) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id`, `usuario_id`, `nombrecompleto`, `identidad`, `celular`, `localidad`, `domicilio`, `direccion`, `coste`, `estado`, `fecha`, `hora`) VALUES
(1, 1, 'javier castillo rosa', 13577413, 12345678, 'Entre Rios', '14 de  septiembre', 'calle 10 de noviembre', 99.00, 'preparation', '2019-06-26', '19:23:10'),
(3, 1, 'xavier castillo rosa', 13577413, 13465663, 'Entre Rios', '10 de noviembre', '10 de nobiembre', 75.00, 'confirm', '2019-11-03', '08:02:34'),
(4, 1, 'xavier castillo rosa', 13577413, 13465663, 'Entre Rios', '10 de noviembre', '10 de nobiembre', 75.00, 'confirm', '2019-11-03', '08:03:04'),
(5, 1, 'xavier castillo rosa', 13577413, 13465663, 'Cruce Andino', '10 de noviembre', '14 de setiembre', 189.00, 'confirm', '2019-11-03', '08:08:58'),
(6, 2, 'juan castillo rosa', 13477413, 13465663, 'Entre Rios', '10 de noviembre', '10 de nobiembre', 588.99, 'confirm', '2019-11-03', '10:29:03'),
(7, 1, 'xavier castillo rosa', 13577413, 13577413, 'Manco Kapac', 'bario 14 de septiembre', 'calle 10 de noviembre', 1584.00, 'sended', '2019-11-12', '13:17:21'),
(8, 1, 'xavier', 1234567, 12345678, 'Gualberto Villarroel', '14 de septiembre', 'calle 10 de novi..', 345.00, 'confirm', '2019-12-03', '14:35:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(255) NOT NULL,
  `categoria_id` int(255) NOT NULL,
  `subcategoria_id` int(255) NOT NULL,
  `nombre` varchar(500) NOT NULL,
  `descripcion` text DEFAULT NULL,
  `precio` float(100,2) NOT NULL,
  `stock` int(255) NOT NULL,
  `oferta` varchar(4) DEFAULT NULL,
  `fecha` date NOT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `imagen1` varchar(255) DEFAULT NULL,
  `imagen2` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `categoria_id`, `subcategoria_id`, `nombre`, `descripcion`, `precio`, `stock`, `oferta`, `fecha`, `imagen`, `imagen1`, `imagen2`) VALUES
(1, 1, 1, 'Polo de manga corta con cuello y manga de color sólido en color blanco122', '    Season:\r\n\r\n    Spring,Summer,Autumn\r\n\r\n    Sleeve:\r\n\r\n    Short sleeves\r\n\r\n    Pattern:\r\n\r\n    Stripe\r\n\r\n    Material:\r\n\r\n    Cotton Blends\r\n\r\n    Style:\r\n\r\n    Casual style,Work style,Fashion style,Business style\r\n\r\n    Neckline:\r\n\r\n    Lapel collar\r\n\r\n    Type:\r\n\r\n    T-Shirt\r\n\r\n    Detail:\r\n\r\n    Screw thread\r\n\r\n    Length:\r\n\r\n    Normal length\r\n\r\n    Fit:\r\n\r\n    Regular fit\r\n\r\n    Size:\r\n\r\n    S,M,L,XL,XS,XXS\r\n\r\n    Button Design:\r\n\r\n    Front button\r\n\r\n    Color:\r\n\r\n    White\r\n\r\n    Hem:\r\n\r\n    Bodycon Hem\r\n\r\n', 99.00, 6, NULL, '2019-06-26', 'camisa1.jpg', 'camisa3.jpg', 'chaquetas1.jpg'),
(2, 2, 2, ' Vestido de fiesta Vintage para mujer vestido elegante de manga larga de lunares azul sólido', '<p>El vestido es muy simple y elegante, puedes llevarlo para participar en la fiesta de fin de semana...</p>\r\n<p>Nota:El tamaño del vestido es un poco pequeño, se recomienda comprar un vestido de mayor tamaño.</p>', 484.99, 3, NULL, '2019-06-27', 'vestido de fiesta 1.webp', 'vestido de fiesta 2.jpg', 'vestido de fiesta 3.webp'),
(3, 1, 1, 'Camisa  con rayas para hombre', 'Composición y cuidados\r\n\r\nMaterial exterior: 100% algodón\r\n\r\nCuidados: Lavar a máquina a 40 °C, no utilizar secadora, programa delicado\r\n\r\nCaracterísticas del producto\r\n\r\nCuello: Kent\r\n\r\nCierre: Botón\r\n\r\nEstampado: De cuadros\r\n\r\nNúmero de artículo: 4BE22D043-K17', 129.00, 6, NULL, '2019-06-27', 'camisa ralla1.jpg', 'camisa ralla2.jpg', 'camisa ralla3.jpg'),
(4, 1, 3, 'MICK - Chaquetas bomber  para hombre', 'Composición y cuidados\r\n\r\nMaterial exterior: 65% poliéster, 35% algodón\r\n\r\nRelleno: 65% poliéster, 35% algodón\r\n\r\nCuidados: Lavar a máquina a 30°C\r\n\r\nCaracterísticas del producto\r\n\r\nCuello: Alzado\r\n\r\nCierre: Cremallera\r\n\r\nBolsillos: Con solapa, bolsillo interior\r\n\r\nEstampado: Unicolor\r\n\r\nDetalles: Cintura elástica\r\n\r\nNúmero de artículo: HA422T00B-Q11', 269.00, 2, NULL, '2019-06-27', 'chaquetas1.jpg', 'chaquetas2.jpg', 'P535765-1.jpg'),
(5, 3, 4, 'Ratón inalámbrico  silenciosa Mause recargable ergonómico ratón USB de 2,4 Ghz ', 'Ratón inalámbrico de la computadora Mouse Bluetooth PC silenciosa Mause recargable ergonómico ratón USB de 2,4 Ghz ratón óptico para PC portátil \r\n\r\n1Nuevo y mejorado:Tecnología de carga única, b Batería de litio ult-in\r\n2Diseño silencioso: El clic izquierdo y derecho son ambos silencio, no hay sonido cuando haces clic\r\n3, 4 Las llavesMás conveniente para las operaciones del juego\r\n43 tipos DPI Ajustable Ofrece posicionamiento preciso\r\n5. Recargable: Batería recargable integrada, 2-3 meses de uso después de 3-4 horas de carga\r\n\r\n \r\n\r\n\r\n\r\nNombre del artículo: ratón inalámbrico 2,4G\r\nBotones: 4 botones\r\nEl Max DPI: 24 00 DPI\r\nAjustable DPI interruptor: 12 00/1600/ 24 00 DPI\r\nLa gama WiFi: 10 M\r\nBatería: batería recargable\r\nLongitud del Cable de carga: unos 50 cm\r\nVida útil del interruptor: 10 M ILLONES T Imes\r\nTamaño: 112 * 57 * 20mm\r\nSistema de soporte: Windows/ Mac\r\n \r\n<ul>\r\n<li>1 ratón inalámbrico 2,4G<7li>\r\n<li>\r\n1 x receptor USB<7li>\r\n<li>\r\n1 x Cable de carga\r\n <7li>\r\n</ul\r\n\r\n\r\n\r\n\r\nCuando el puntero del ratón no puede localizarlo con precisión, es necesario cargarlo.\r\nCuando se carga, la rueda de desplazamiento es azul claro.\r\n ', 59.00, 12, NULL, '2019-06-28', 'raton2.jpg', 'raton1.jpg', 'raton3.jpg'),
(6, 1, 3, 'Polera con capucha Trifolio Solid - Negro adidas | adidas Bolivia\r\n', 'Polera con capucha Trifolio Solid - Negro adidas | adidas Peru\r\n', 345.00, 12, NULL, '2019-08-04', 'chaqueta1.jpeg', 'chaqueta2.jpeg', 'chaqueta3.jpg'),
(7, 1, 5, 'Zapatos de senderismo de alta calidad  para hombres casuales calientes zapatos de montaña escalada para hombres', '<h6>Zapatos de senderismo de alta calidad para hombres al aire libre impermeables 2019 Otoño e Invierno zapatos de Trekking para hombres casuales calientes zapatos de montaña escalada para hombres</h6>\r\n\r\n<p>Cuando realices un pedido, presta atención a los siguientes puntos, ¡Muchas gracias! Puntera: redonda tamaño de la cabeza: UE 39 40 41 42 43 44 45 46 47 paquete: sin caja, para reducir el peso del paquete Color: gris, marrón, verde militar</p>\r\n\r\n<h6>Gráfico de tamaño</h6>\r\n\r\n<p>Porque las personas en diferentes países tienen diferentes tamaños de zapatos, así que si tu pie es más ancho o más grueso, te recomendamos que elijas un tamaño más grande para elegir el tamaño más adecuado para ti, la mejor manera es que me diga la longitud de su pie, ¡y luego elijo la talla para usted!</p>\r\n\r\n<img src=\"https://ae01.alicdn.com/kf/HTB1jEUGXpY7gK0jSZKzq6yikpXab.jpg?width=800&height=800&hash=1600\">', 249.99, 2, NULL, '2019-08-06', 'Zapatos de senderismo1.jpg', 'Zapatos de senderismo2.jpg', 'Zapatos de senderismo3.jpg'),
(8, 1, 5, ' Zapatillas de malla para Hombre, zapatos casuales ligeros, cómodos, transpirables Talla 39', '<img src=\"https://ae01.alicdn.com/kf/H6f12293dedb84b05ada644d6728d3b035.jpg\">', 169.99, 6, 'NULL', '2019-11-03', 'Zapatillas de malla para1.jpg', 'Zapatillas de malla para2.webp', 'Zapatillas de malla para3.webp'),
(9, 2, 2, 'Vestidos de playa de verano florales con lazo y lazo para mujer Talla S', '	\r\n\r\n<p>La calidad es la primera con el mejor servicio. Todos los clientes son nuestros amigos.\r\n¿Color? Blanco con estampado de Mariposa (como en la imagen)</p>\r\n\r\n<ul>\r\n<li>Material: Material de poliéster</li>\r\n\r\n<li>Con cinturón: Sí</li>\r\n\r\n<li>Forma del cuello: Ribbed</li>\r\n\r\n<li>Silueta: Línea A</li>\r\n\r\n<li>Longitud del vestido: hasta la rodilla</li>\r\n\r\n<li>Temporada: primavera, verano</li>\r\n\r\n<li>ESTILO: moda, Rockabilly, fiesta, oficina, vacaciones</li>\r\n<ul>', 80.00, 3, 'NULL', '2019-11-03', 'Vestidos de playa1.jpg', 'Vestidos de playa2.jpg', 'Vestidos de playa3.jpg'),
(10, 4, 16, '2 Pack para Samsung Galaxy Note 9 adaptador de carga USB-C tipo C ', 'r', 22.50, 12, 'NULL', '2019-11-10', 'electronica-c1.jpg', 'electronica-c2.jpg', 'electronica-c3.jpg'),
(11, 5, 23, 'Coca Cola mini de 250Ml ', 'todo', 2.50, 12, NULL, '2019-11-16', 'cocacola.jpg', 'colacola2.jpg', 'cocacola3.jpg'),
(12, 5, 23, 'Tropi Frut de un 1 Litro', 'todo', 12.99, 12, 'NULL', '2019-11-16', 'tropi1.jpg', 'tropi2.png', 'tropi3.png'),
(13, 6, 26, 'Acero inoxidable vajilla 24 Uds Negro Set de cubiertos, tenedor cuchara cuchillo occidental ', 'Juego de vajilla 24 Uds de alta calidad 8 colores  \r\n\r\n\r\nNombre del producto: cubiertos de acero inoxidable de 24 Uds.\r\nObra de arte: Espejo polaco\r\nMaterial:Acero inoxidable 18/8 (material de calidad alimentaria)\r\nPeso: 173,2 g/Set\r\nPaquete incluido: 24 piezas conjunto = 6 cuchillos para la cena + 6 tenedor para la cena + 6 cucharas para la cena + 6 cucharas para el té\r\n\r\nNota: Debido a la luz y la resolución de la pantalla u otras razones, las imágenes y los objetos pueden variar ligeramente de color, por favor prevalece. Por favor, asegúrate de que no te importa antes de comprar.', 34.99, 12, 'NULL', '2019-11-16', 'cosina1.jpg', 'cosina2.jpg', 'cosina1.jpg'),
(14, 2, 2, 'Polka  Vestido Mujer blanco espalda descubierta mini vestidos de fiesta manga larga otoño verano ', '<img src=\"https://ae01.alicdn.com/kf/Hd98ae5386a224599a4bbe053f466afaa3.jpg\">\r\n\r\n<img src=\"https://ae01.alicdn.com/kf/HTB1SynOboT1gK0jSZFrq6ANCXXaV.jpg\">', 449.99, 2, 'NULL', '2019-11-21', 'polka dot Vestido 1.jpg', 'polka dot Vestido 2.webp', 'polka dot Vestido 3.webp'),
(15, 2, 38, 'Blusa de chifón de manga larga informal para mujer camisa Sexy con cuello en V para mujer', '<img src=\"https://ae01.alicdn.com/kf/HTB1H9r0bijrK1RjSsplq6xHmVXaw.jpg\">', 79.99, 3, 'NULL', '2019-11-21', 'Blusa de chifón 1.webp', 'Blusa de chifón 2.webp', 'Blusa de chifón 3.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategorias`
--

CREATE TABLE `subcategorias` (
  `id` int(255) NOT NULL,
  `categoria_id` int(255) DEFAULT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subcategorias`
--

INSERT INTO `subcategorias` (`id`, `categoria_id`, `nombre`) VALUES
(1, 1, 'Camisas'),
(2, 2, 'Vestidos'),
(3, 1, 'Chaquetas y Abrigos'),
(4, 3, 'Ratones'),
(5, 1, 'Zapatos'),
(6, 4, 'Camaras'),
(7, 4, 'Auriculares y Cascos'),
(8, 3, 'Placas Bases'),
(9, 3, 'Tarjetas Graficas'),
(10, 3, 'Portatiles'),
(11, 3, 'Tables'),
(12, 3, 'Unidades Flash USB'),
(13, 3, 'Tarjetas de Menoria'),
(14, 3, 'Discos Duros HDD SSD'),
(15, 4, 'Baterias'),
(16, 4, 'Cables y Adadtadores'),
(17, 4, 'Bolsas y Fundas'),
(18, 4, 'Tilevisores'),
(19, 4, 'Proyectores'),
(20, 4, 'Altavoces'),
(21, 5, 'Lactios'),
(22, 5, 'Reposteria'),
(23, 5, 'Jugos y Refrescos'),
(24, 5, 'Restaurantes'),
(25, 5, 'Bebedas alcoholicas'),
(26, 6, 'Utensilios para el Hogar'),
(27, 6, 'Vasos, Copas y Tasas'),
(28, 6, 'Ropa de Cama'),
(29, 6, 'Relojes de Pared'),
(30, 1, 'Pantalones Rasgados'),
(31, 1, 'Calsetines'),
(32, 1, 'Ropa Interior'),
(33, 1, 'Deporte'),
(34, 2, 'Pantalones Cortos y Short'),
(35, 2, 'Faldas'),
(36, 2, 'Leggings'),
(37, 2, 'Deportes'),
(38, 2, 'Blusas y Camisas'),
(39, 2, 'Chaqueta y Abrigos'),
(40, 2, 'Jeans');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(255) NOT NULL,
  `nombres` varchar(100) NOT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `celulaIdentidad` int(255) DEFAULT NULL,
  `celular` int(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `rol` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombres`, `apellidos`, `email`, `celulaIdentidad`, `celular`, `password`, `rol`) VALUES
(1, 'fulano', ' rosas', 'fulano123@gmail.com', 13577456, 12345678, '$2y$04$A9JIJj5Ha9wcBDjUQZmtDOWB4PbGP04LGoQSGuqI2.JXkoyGN8TkG', 'admin'),
(2, 'juan', 'sanchez berzain', 'sanchez@gmail.com', 12345678, 12345678, '$2y$04$EPXhJwlusizOh6uFxvvytez3zE6pY9.wym63xvV7oo7cps52El9Xu', 'user'),
(4, 'xavier', 'castillo', 'xavier211999@gmail.com', 12345678, 87654321, '$2y$04$ZutF1Rkub7dx5iC3aCJhheHHXwmvcmvmDASJJTMTxViNr69.z.QsO', 'admin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lineas_pedidos`
--
ALTER TABLE `lineas_pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_linea_pedido` (`pedido_id`),
  ADD KEY `fk_linea_producto` (`producto_id`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pedido_usuario` (`usuario_id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_producto_categoria` (`categoria_id`),
  ADD KEY `fk_producto_subcategoria` (`subcategoria_id`);

--
-- Indices de la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sub_categoria` (`categoria_id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `lineas_pedidos`
--
ALTER TABLE `lineas_pedidos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `lineas_pedidos`
--
ALTER TABLE `lineas_pedidos`
  ADD CONSTRAINT `fk_linea_pedido` FOREIGN KEY (`pedido_id`) REFERENCES `pedidos` (`id`),
  ADD CONSTRAINT `fk_linea_producto` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`);

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `fk_pedido_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `fk_producto_categoria` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`),
  ADD CONSTRAINT `fk_producto_subcategoria` FOREIGN KEY (`subcategoria_id`) REFERENCES `subcategorias` (`id`);

--
-- Filtros para la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  ADD CONSTRAINT `fk_sub_categoria` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
